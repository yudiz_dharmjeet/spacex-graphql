import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import AddUser from '../pages/AddUser/AddUser'
import Home from '../pages/Home/Home'

function index () {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/'>
          <Route index element={<Home />} />
          <Route path='mutation' element={<AddUser />} />
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export default index
