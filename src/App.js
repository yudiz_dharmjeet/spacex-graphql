import React from 'react'
import Routes from './routes'

import './App.scss'
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client'
import NavbarComponent from './components/NavbarComponent/NavbarComponent'
import FooterComponent from './components/FooterComponent/FooterComponent'

function App () {
  const client = new ApolloClient({
    uri: 'https://api.spacex.land/graphql/',
    cache: new InMemoryCache()
  })

  return (
    <div className="main">
      <div className="app">
        <ApolloProvider client={client}>
          <div className='home'>
            <NavbarComponent />

            <Routes />

            <FooterComponent />
          </div>
        </ApolloProvider>
      </div>
    </div>
  )
}

export default App
