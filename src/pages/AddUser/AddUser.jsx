import { useMutation } from '@apollo/client'
import React, { useState } from 'react'
import { ADD_USER } from '../../graphql/addUser'

import './AddUser.scss'

function AddUser () {
  const [user, setUser] = useState('')

  const [addUser, { data }] = useMutation(ADD_USER)

  function addingUser (e) {
    e.preventDefault()
    addUser({
      variables: {
        objects: [
          {
            name: user
          }
        ]
      }
    })
  }

  console.log(data)

  return (
    <div className="middle">
      <h2 className='mt-3'>Add User</h2>

      <form onSubmit={(e) => addingUser(e)}>
        <input type="text" name="name" id="name" onChange={(e) => setUser(e.target.value)} />
        <button className='add-btn' type="submit">Submit</button>
      </form>
    </div>
  )
}

export default AddUser
