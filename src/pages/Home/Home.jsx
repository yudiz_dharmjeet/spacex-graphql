import { useQuery } from '@apollo/client'
import React from 'react'

import { GET_ROCKETS } from '../../graphql/rockets'
import './Home.scss'

function Home () {
  const { loading, error, data } = useQuery(GET_ROCKETS)

  if (error) return <div>Error!</div>

  return (
      <div className="middle">
        {loading
          ? <h2>Loading...</h2>
          : data.rockets.map((item) => {
            return (
              <div key={item.id} className="card mt-3 mb-3" style={{ width: '18rem' }}>
                <div className="card-body">
                  <h3 className="card-title">{item.name}</h3>
                  <p className="card-text">{item.description}</p>
                </div>
              </div>
            )
          })
        }
      </div>
  )
}

export default Home
