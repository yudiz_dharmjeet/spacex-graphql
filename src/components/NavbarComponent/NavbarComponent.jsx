import React from 'react'
import { Nav, Navbar, NavbarBrand, NavItem, NavLink } from 'reactstrap'

import './NavbarComponent.scss'

function NavbarComponent () {
  // const [isOpen, setIsOpen] = useState(false)

  // function toggle () {
  //   setIsOpen(!isOpen)
  // }

  return (
    <div className="header">
      <Navbar className='navbar' color="light" light expand="md">
        <NavbarBrand href="/">SpaceX</NavbarBrand>
        {/* <NavbarToggler onClick={toggle} /> */}
        {/* <Collapse isOpen={true} navbar> */}
          <Nav className="ml-auto d-flex" navbar>
            <NavItem>
              <NavLink href="/mutation">Users</NavLink>
            </NavItem>
          </Nav>
        {/* </Collapse> */}
      </Navbar>
    </div>
  )
}

export default NavbarComponent
