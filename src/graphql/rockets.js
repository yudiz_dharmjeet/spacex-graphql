import { gql } from '@apollo/client'

export const GET_ROCKETS = gql`
  query GetRockets {
    rockets {
      id
      name
      country
      cost_per_launch
      company
      active
      first_flight
      wikipedia
      description
    }
  }
`
