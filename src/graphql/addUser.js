import { gql } from '@apollo/client'

export const ADD_USER = gql`
  mutation Mutation($objects: [users_insert_input!]!) {
    insert_users(objects: $objects) {
      returning {
        id
        name
        rocket
      }
    }
  }
`
